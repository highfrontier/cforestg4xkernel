# Cforestg4x Kernel

Python Kernel for Jupyter with Cforestg4x.

## Merged into cforestg4x

**This repo is no longer maintained, as it has been [merged into cforestg4x itself](https://github.com/root-mirror/cforestg4x#jupyter)**

## Old stuff:

You will probably need to specify the path to cforestg4x with the `CFORESTG4X_EXE` env variable.

**Note:** This currently requires master of everything IPython and Jupyter because that's what I use,
but I'll clean that up so it works on 3.x.

## Install

To install the kernel:

    jupyter kernelspec install cforestg4x

or for IPython/Jupyter < 4:

    ipython kernelspec install cforestg4x
